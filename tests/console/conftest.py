import pytest

from foobartory.console.application import Application


@pytest.fixture()
def app():
    return Application()
