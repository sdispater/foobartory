# Foobartory

## Installation

You will need Poetry to start using the Foobartory codebase. Refer to the [documentation](https://python-poetry.org/docs/#introduction) to start using Poetry.

You will first need to clone the repository using git and place yourself in its directory:

```bash
$ git clone git@bitbucket.org:sdispater/foobartory.git
$ cd foobartory
```

Now, you will need to install the required dependency for Foobartory and be sure that the current tests are passing on your machine:

```bash
$ poetry install
$ poetry run pytest tests/
```

## Usage

Once Foobartory is installed you can start using by invoking the `foobartory` script.

The `run` command will start the foorbartory automatisation:

```bash
$ foobartory run
```

By default, the robots will wait actual seconds to execute their tasks or to change their activity.
If you want to speed it up you can use the `--d/duration-factor` option to apply a factor.

```bash
$ foobartory run -d 0.1  # Divides the wait times by 10
$ foobartory run --duration-factory 2.0  # Doubles the wait time
```

Similarly, the default goal for the foobartory is to have 30 robots.
This goal can be adjusted by using the `--t/target` option:

```bash
$ foobartory run -t 10
$ foobartory run --target 10
```

## Overview

The project respects the initial instructions by having a fully automated foobartory.

However, it is rather slow and there is a lot of room for improvements (see below).

I tried to have a code architecture and implementation that favors extensibility – by using dependency injection, for instance.

Tests have been written – mostly a big integration test for the `run` command - that give a satisfying coverage (around 93%).

## Possible improvements

The strategy used by the supervisor is pretty naive
and by using actual computation logic we could implement a faster strategy.

Gather statistics about the robots tasks (like failure rate) could be added for a better reporting.

The robots currently work sequentially but it would be better to make them work in parallel.
However, to make it work the code and supervisor strategy would most likely need to be changed.

Improve test coverage (currently at around 93%) by writing more unit tests.
