from typing import List

from cleo.io.io import IO

from foobartory.robot import Activity
from foobartory.robot import Robot
from foobartory.stock import Stock

from .reporter import Reporter


class ConsoleReporter(Reporter):
    def __init__(self, io: IO) -> None:
        self._io = io

    def report_activity(self, robot: Robot, activity: Activity) -> None:
        self._io.write_line(
            "  <c1>{}</c1>: working on the <c2>{}</c2> task".format(
                robot, activity.value
            )
        )

    def report_activity_change(self, robot: Robot, activity: Activity) -> None:
        if robot.current_activity and activity != robot.current_activity:
            self._io.write_line(
                "  <c1>{}</c1>: <warning>changing</warning> task from <c2>{}</c2> to <c2>{}</c2>".format(
                    robot, robot.current_activity.value, activity.value
                )
            )

    def report_summary(self, stock: Stock, robots: List[Robot]) -> None:
        self._io.write_line("")

        self._io.write_line(
            "  <b>Summary</>: "
            "<fg=cyan;options=bold>{}</> <fg=cyan>robots</>, "
            "<fg=green;options=bold>{}</> <fg=green>euros</>, "
            "<b>{}</b> foobar, "
            "<b>{}</b> foo, "
            "<b>{}</b> bar"
            "".format(
                len(robots),
                stock.stock("cash"),
                stock.stock("foobar"),
                stock.stock("foo"),
                stock.stock("bar"),
            )
        )
