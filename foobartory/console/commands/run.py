from cleo.commands.command import Command
from cleo.helpers import option


class RunCommand(Command):

    name = "run"

    description = "Run the Foobartory"

    options = [
        option(
            "duration-factor",
            "d",
            description="The factor used to adjust wait times for robots",
            flag=False,
            default=1.0,
        ),
        option(
            "target",
            "-t",
            description="The target number of robots we want",
            flag=False,
            default=30,
        ),
    ]

    def handle(self) -> int:
        from foobartory.reporters.console_reporter import ConsoleReporter
        from foobartory.supervisor import Supervisor

        configuration = {"duration_factor": float(self.option("duration-factor"))}

        self.line("")

        supervisor = Supervisor(ConsoleReporter(self._io))
        supervisor.start(configuration=configuration, target=int(self.option("target")))

        return 0
