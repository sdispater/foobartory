from importlib import import_module
from typing import Callable
from typing import Optional
from typing import Type

from cleo.application import Application as _Application
from cleo.commands.command import Command
from cleo.formatters.style import Style
from cleo.io.inputs.input import Input
from cleo.io.io import IO
from cleo.io.outputs.output import Output
from cleo.loaders.factory_command_loader import FactoryCommandLoader

from foobartory import __version__


def load_command(name: str) -> Callable:
    def _load() -> Type[Command]:
        module = import_module(
            "foobartory.console.commands.{}".format(".".join(name.split(" ")))
        )
        command_class = getattr(
            module, "{}Command".format("".join(c.title() for c in name.split(" ")))
        )

        return command_class()

    return _load


COMMANDS = ["run"]


class Application(_Application):
    def __init__(self) -> None:
        super().__init__(name="Foobartory", version=__version__)

        self.set_command_loader(
            FactoryCommandLoader({name: load_command(name) for name in COMMANDS})
        )

    def create_io(
        self,
        input: Optional[Input] = None,
        output: Optional[Output] = None,
        error_output: Optional[Output] = None,
    ) -> IO:
        io = super().create_io(input, output, error_output)

        formatter = io.output.formatter
        formatter.set_style("c1", Style("cyan"))
        formatter.set_style("c2", Style("default", options=["bold"]))
        formatter.set_style("warning", Style("yellow"))
        formatter.set_style("debug", Style("default", options=["dark"]))
        formatter.set_style("success", Style("green"))

        return io


def main() -> int:
    return Application().run()


if __name__ == "__main__":
    main()
