import re

from cleo.formatters.style import Style
from cleo.testers.command_tester import CommandTester


def test_run_launches_a_supervisor(app):
    tester = CommandTester(app.find("run"))
    tester.io.output.formatter.set_style("warning", Style("yellow"))

    tester.execute("-t 5 -d 0")

    output = tester.io.fetch_output()

    assert re.search("Robot (\d+): working on the (\w+) task", output) is not None
    assert (
        re.search("Robot (\d+): changing task from (\w+) to (\w+)", output) is not None
    )
    assert (
        re.search(
            "Summary: 5 robots, (\d+) euros, (\d+) foobar, (\d+) foo, (\d+) bar",
            output,
        )
        is not None
    )
