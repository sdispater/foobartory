import random

from typing import Any
from typing import Dict
from typing import List
from typing import Optional

from foobartory.reporters.reporter import Reporter

from .robot import Activity
from .robot import Robot
from .stock import Stock


class Supervisor:
    def __init__(self, reporter: Reporter) -> None:
        self._robots: List[Robot] = []
        self._available_robots: Dict[Activity, List[Robot]] = {
            activity: [] for activity in Activity
        }
        self._stock: Stock = Stock()
        self._reporter: Reporter = reporter

    def start(
        self,
        robots: int = 2,
        configuration: Optional[Dict[str, Any]] = None,
        target: int = 30,
    ) -> None:
        if configuration is None:
            configuration = {}

        for i in range(robots):
            robot = Robot(str(i + 1), configuration=configuration)
            self._robots.append(robot)
            self._available_robots[Activity.MINE_FOO].append(robot)

        self._supervise(configuration, target=target)

    def _supervise(self, configuration: Dict[str, Any], target: int) -> None:
        while len(self._robots) < target:
            self._assign(configuration)

        self._reporter.report_summary(self._stock, self._robots)

    def _assign(self, configuration: Dict[str, Any]) -> None:
        """
        Assign an activity to an available robot.
        """
        activity = Activity.MINE_FOO

        if self._stock.stock("cash") >= 3 and self._stock.stock("foo") >= 6:
            activity = Activity.BUY_ROBOT
        elif self._stock.stock("foobar") >= 1 and self._stock.stock("cash") < 3:
            activity = Activity.SELL_FOOBAR
        elif (
            self._stock.stock("foo") >= 1
            and self._stock.stock("bar") >= 1
            and self._stock.stock("cash") < 3
            and self._stock.stock("foo") >= 7
        ):
            activity = Activity.ASSEMBLE_FOOBAR
        elif self._stock.stock("foo") > self._stock.stock("bar") * 3:
            activity = Activity.MINE_BAR

        robot = None
        if not self._available_robots[activity]:
            # No available robot for the current activity,
            # so we try to find one available elsewhere
            priorities = [
                Activity.MINE_FOO,
                Activity.MINE_BAR,
                Activity.ASSEMBLE_FOOBAR,
                Activity.SELL_FOOBAR,
                Activity.BUY_ROBOT,
            ]

            prioritary_activity = random.choice(priorities)

            if self._available_robots[prioritary_activity]:
                robot = self._available_robots[prioritary_activity].pop(0)
        else:
            robot = self._available_robots[activity].pop(0)

        if robot is None:
            return

        self._reporter.report_activity_change(robot, activity)
        self._reporter.report_activity(robot, activity)
        result = robot.work_on(activity, self._stock)

        if activity == Activity.BUY_ROBOT:
            result.set_name(str(len(self._robots) + 1))
            result.set_configuration(configuration)
            self._robots.append(result)
            self._available_robots[Activity.MINE_FOO].append(result)

        self._available_robots[activity].append(robot)
