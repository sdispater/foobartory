import logging
import random
import time

from enum import Enum
from typing import Any
from typing import Dict
from typing import Optional

from .stock import Stock


logger = logging.getLogger(__name__)


class Activity(Enum):

    MINE_FOO = "mine_foo"
    MINE_BAR = "mine_bar"
    ASSEMBLE_FOOBAR = "assemble_foobar"
    SELL_FOOBAR = "sell_foobar"
    BUY_ROBOT = "buy_robot"


class Robot:

    ACTIVITES = ["mine_foo", "mine_bar", "assemble"]

    def __init__(
        self, name: Optional[str] = None, configuration: Optional[Dict[str, Any]] = None
    ) -> None:
        self._name: Optional[str] = name
        self._current_activity: Optional[Activity] = None

        if configuration is None:
            configuration = {}

        self._configuration: Dict[str, Any] = configuration

    @property
    def current_activity(self) -> Activity:
        return self._current_activity

    def set_name(self, name: str) -> None:
        self._name = name

    def set_configuration(self, configuration: Dict[str, Any]) -> None:
        self._configuration = configuration

    def work_on(self, activity: Activity, *args, **kwargs) -> None:
        if self._current_activity and self._current_activity != activity:
            self._wait(5.0)

        self._current_activity = activity

        return getattr(self, activity.value)(*args, **kwargs)

    def mine_foo(self, stock: Stock) -> None:
        self._wait(1)

        stock.add("foo")

    def mine_bar(self, stock: Stock) -> None:
        self._wait(random.uniform(0.5, 2.0))

        stock.add("bar")

    def assemble_foobar(self, stock: Stock) -> bool:
        stock.retrieve("foo")
        stock.retrieve("bar")

        self._wait(2)

        # The assembly has a 60% chance of success
        if random.random() < 0.6:
            # Upon success, add a new foobar to the stock
            stock.add("foobar")
        else:
            # Upon failure, the bar is lost but the foo can be reused
            stock.add("foo")

    def sell_foobar(self, stock: Stock) -> int:
        self._wait(10)

        foobars_count = min(5, stock.stock("foobar"))

        stock.retrieve("foobar", number=foobars_count)

        stock.add("cash", foobars_count)

        return foobars_count

    def buy_robot(self, stock: Stock) -> "Robot":
        stock.retrieve("cash", 3)
        stock.retrieve("foo", 6)

        return self.__class__()

    def _wait(self, wait_time: int) -> None:
        factor = self._configuration.get("duration_factor", 1.0)

        time.sleep(wait_time * factor)

    def __str__(self) -> str:
        return f"Robot {self._name}"
