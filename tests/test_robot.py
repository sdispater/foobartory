import random

import pytest

from foobartory.robot import Activity
from foobartory.robot import Robot
from foobartory.stock import Stock


@pytest.fixture()
def robot():
    configuration = {"duration_factor": 0}

    return Robot("foo", configuration=configuration)


@pytest.fixture()
def stock():
    stock = Stock()

    return stock


def test_robot_mines_foo(robot, stock):
    current_stock = stock.stock("foo")

    robot.mine_foo(stock)

    assert stock.stock("foo") == current_stock + 1


def test_robot_mines_bar(robot, stock):
    current_stock = stock.stock("bar")

    robot.mine_bar(stock)

    assert stock.stock("bar") == current_stock + 1


def test_robot_assembles_foobar_with_success(robot, stock, mocker):
    # Patch random.random to ensure success
    mocker.patch.object(random, "random", return_value=0.1)

    stock.add("foo")
    stock.add("bar")
    current_foo_stock = stock.stock("foo")
    current_bar_stock = stock.stock("bar")
    current_foobar_stock = stock.stock("foobar")

    robot.assemble_foobar(stock)

    assert stock.stock("foo") == current_foo_stock - 1
    assert stock.stock("bar") == current_bar_stock - 1
    assert stock.stock("foobar") == current_foobar_stock + 1


def test_robot_assembles_foobar_with_failure(robot, stock, mocker):
    # Patch random.random to ensure failure
    mocker.patch.object(random, "random", return_value=0.8)

    stock.add("foo")
    stock.add("bar")
    current_foo_stock = stock.stock("foo")
    current_bar_stock = stock.stock("bar")
    current_foobar_stock = stock.stock("foobar")

    robot.assemble_foobar(stock)

    assert stock.stock("foo") == current_foo_stock
    assert stock.stock("bar") == current_bar_stock - 1
    assert stock.stock("foobar") == current_foobar_stock


def test_sells_foobar(robot, stock):
    stock.add("foobar", 6)
    current_foobar_stock = stock.stock("foobar")
    current_cash_stock = stock.stock("cash")

    robot.sell_foobar(stock)

    assert stock.stock("foobar") == current_foobar_stock - 5
    assert stock.stock("cash") == current_cash_stock + 5


def test_buys_robot(robot, stock):
    stock.add("foo", 8)
    stock.add("cash", 5)

    current_foo_stock = stock.stock("foo")
    current_cash_stock = stock.stock("cash")

    assert isinstance(robot.buy_robot(stock), Robot)

    assert stock.stock("foo") == current_foo_stock - 6
    assert stock.stock("cash") == current_cash_stock - 3


def test_work_on_calls_underlying_tasks(robot, stock, mocker):
    mine_foo = mocker.patch.object(Robot, "mine_foo")

    robot.work_on(Activity.MINE_FOO, stock)

    assert mine_foo.called


def test_work_on_waits_on_activity_change(robot, stock, mocker):
    robot.work_on(Activity.MINE_FOO, stock)

    mine_foo = mocker.patch.object(Robot, "_wait")

    robot.work_on(Activity.MINE_BAR, stock)

    assert mine_foo.call_args_list[0] == mocker.call(5.0)
