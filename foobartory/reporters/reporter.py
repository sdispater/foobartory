from typing import List

from foobartory.robot import Activity
from foobartory.robot import Robot
from foobartory.stock import Stock


class Reporter:
    def report_activity(self, robot: Robot, activity: Activity) -> None:
        raise NotImplementedError()

    def report_activity_change(self, robot: Robot, activity: Activity) -> None:
        raise NotImplementedError()

    def report_summary(self, stock: Stock, robots: List[Robot]) -> None:
        raise NotImplementedError()
