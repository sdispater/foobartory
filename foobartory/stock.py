class Stock:
    def __init__(self) -> None:
        self._products = {"foo": 0, "bar": 0, "foobar": 0, "cash": 0}

    def add(self, product: str, number: int = 1) -> None:
        self._products[product] += number

    def retrieve(self, product: str, number: int = 1) -> None:
        if number > self._products[product]:
            raise ValueError(
                f"Not enough {product} in stock. "
                f"({number} were requested but there are only {self._products[product]} available)"
            )

        self._products[product] -= number

    def stock(self, product: str) -> int:
        return self._products[product]
